import Vue from 'vue'
import test from "vue-test"
import mount from "vue-test/mount"
import FormRegister from '../components/forms/FormRegister';

describe(FormRegister, () => {
    it('tiene un hook mounted', () => {
        expect(typeof MyComponent.mounted).toBe('function')
    })

    it('tiene un hook before destroy', () => {
        expect(typeof MyComponent.beforeDestroy).toBe('function')
    })

    it('establece correctamente el mensaje cuando se crea', () => {
        const vm = new Vue(MyComponent).$mount()
        expect(vm.message).toBe('Hello world')
    })

    it('emite el mensaje de exito correcto', () => {
        const Constructor = Vue.extend(MyComponent)
        const vm = new Constructor().$mount()
        expect(vm.$el.textContent).toBe('correct')
    })
})

test('Emitir eventos cuando clickeamos o llamamos una acción', () => {
    const wrapper = mount(FormRegister)

    wrapper.find('button').trigger('click')

    expect(wrapper.emitted('validate')).toHaveErrors(11)

    expect(wrapper.emitted('resetValidatation')).toEqual("resetValidation")

    expect(wrapper.emitted('beforeDestroy')).toEqual("beforeDestroy")
})