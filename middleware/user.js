export default ({ store, redirect }) => {
    // If the user is  authenticated
    if (!store.state.auth.loggedIn) {
        return redirect('/')
    }
    //return redirect('/auth/login')
}