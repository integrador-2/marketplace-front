import Swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.css';

export default ({ app }, inject) => {
    inject("Swiper", Swiper);
}