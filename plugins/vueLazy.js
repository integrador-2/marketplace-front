import VueLazyload from "vue-lazyload";
import Vue from 'vue';
import noImage from '../static/img/no-product-image.png';
import logoML from '../static/logos/logo-color.png';

Vue.use(VueLazyload, {
    preLoad: 1,
    error: logoML,
    loading: noImage,
    attempt: 1
});