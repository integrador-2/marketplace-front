import logoML from '../static/logos/logo-color.png';
import baseURL from '../constant/baseURL';

export default (product) => {
    if (!product) return {
        title: 'Producto no disponible',
    }
    return {
        title: `${product.title} en Mercado de Lima` || 'Mercado de Lima',
        meta: [{
                hid: "product-name",
                name: "name",
                content: product.title,
            },
            {
                hid: "product-description",
                name: "description",
                content: product.productReview ? product.productReview : product.title,
            },
            {
                hid: "product-keyword",
                name: "keyword",
                content: product.title,
            },
            {
                hid: "logo-cdt",
                name: "msapplication-TileImage",
                content: baseURL + logoML,
            },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: product.title
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'article'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: product.images && product.images[0] ? `${baseURL}/file/${product.images[0].filename }` : baseURL + logoML,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: product.productReview ? product.productReview : product.title,
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: `${baseURL}/${product.store.sku}/${product.sku}`,
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: product.title
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: product.images && product.images[0] ? `${baseURL}/file/${product.images[0].filename }` : baseURL + logoML,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: product.productReview ? product.productReview : product.title,
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },
        ]
    }
}